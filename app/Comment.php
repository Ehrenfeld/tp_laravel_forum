<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['content', 'user_id', 'post_id'];

    function user() {

        return $this->belongsTo(User::class);
    }

    static function smiley( $text ) {
        $text = str_replace(' :love: ', ' <i class="far fa-grin-hearts"></i> ', $text);
        $text = str_replace(' ;) ', ' <i class="far fa-smile-wink"></i> ', $text);
        $text = str_replace(' ^^ ', ' <i class="far fa-smile-beam"></i> ', $text);
        $text = str_replace(' :) ', ' <i class="far fa-smile"></i> ', $text);
        $text = str_replace(' :beamsweat: ', ' <i class="far fa-grin-beam-sweat"></i> ', $text);
        $text = str_replace(' x) ', ' <i class="far fa-grin-squint"></i> ', $text);
        $text = str_replace(' xD ', ' <i class="far fa-grin-squint"></i> ', $text);
        $text = str_replace(' x\') ', ' <i class="far fa-grin-squint-tears"></i> ', $text);
        $text = str_replace(' x\'D ', ' <i class="far fa-grin-squint-tears"></i> ', $text);


        echo $text;
    }
}
