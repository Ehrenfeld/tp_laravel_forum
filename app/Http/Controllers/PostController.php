<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('posts.create', compact( 'categories' ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'title' => 'required|max:50',
            'content' => 'required',
            'categories' => 'required'
        ]);


        $post = Post::create( [
            'title' => $fields['title'],
            'content' => $fields['content'],
            'user_id' => Auth::user()->id,
            'category_id' => $fields['categories']['0']
        ] );

        //$post->addCategories( $fields['categories']);

        return redirect(route('home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post = Post::find($post->id);

        $user = User::find($post->user_id);

        $comments = Comment::all();

        $comments = $comments->where('post_id', $post->id);

        return view('posts.show', compact('post', 'user', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $post = Post::find($post->id);

        $categories = Category::all();
        return view('posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        /*if (Auth::user()->id != $post->id)
            return back();*/

        $fields = $request->validate([
            'title' => 'required|max:50',
            'content' => 'required',
            'categories' => 'required'
        ]);

        $post->update( [
            'title' => $fields['title'],
            'content' => $fields['content'],
            'category_id' => $fields['categories']['0']] );

        return redirect(route('posts.show', $post));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        //if (Auth::user()->id != $post->id)
          //  return back();

        $post->delete();
        return redirect(route('home'));
    }
}
