<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'content', 'user_id', 'category_id'];

    function categories() {

        return $this->belongsTo(Category::class);
    }


    function addCategories( array $categories ) {

        $this->categories()->attach( $categories );
    }

}
