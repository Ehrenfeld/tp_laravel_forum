<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 15)->create();
        factory(App\Category::class, 15)->create();
        factory(App\Post::class, 60)->create();
        factory(App\Comment::class, 100)->create();

        $categories = App\Category::all();

        foreach (App\Post::all() as $post) {

            $link_categories = $categories->random( rand(1, 3));

            foreach ( $link_categories as $link_category )

                $post->categories()->attach($link_category);
        }
    }
}
