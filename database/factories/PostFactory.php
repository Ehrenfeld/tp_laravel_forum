<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'content' => $faker->text(1000),
        'user_id' => App\User::all()->random(),
        'category_id' => App\Category::all()->random()
    ];
});