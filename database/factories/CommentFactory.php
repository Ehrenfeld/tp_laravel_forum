<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'content' => $faker->text(100),
        'user_id' => App\User::all()->random(),
        'post_id' => App\Post::all()->random()
    ];
});