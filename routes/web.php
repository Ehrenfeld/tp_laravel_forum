<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');

Route::resource('categories', 'CategoryController')
    ->parameters([
        'category' =>'[0-9]+'
    ]);

Route::resource('posts', 'PostController')
    ->parameters([
        'post' =>'[0-9]+'
    ]);

/*Route::resource('comments', 'CommentController')->onlu
    ->parameters([
        'comment' =>'[0-9]+'
    ]);*/

Route::resource('/posts/{post}/comments', 'CommentController')->only(['store', 'destroy']);