@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h2>All Categories: </h2>
        <div class="col-md-8 flex-div">
            @foreach ($categories as $category)

            <div class="card" style="width: 18rem;">
                <div class="card-body div-body">
                    <h5 class="card-title">{{$category->label}}</h5>
                    <a href="{{route('categories.show', $category->id)}}" class="card-link"><button type="button" class="btn btn-outline-success">See posts in this Category</button></a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
@endsection
