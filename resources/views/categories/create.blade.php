@extends('layouts.app')

@section('content')
    <div class="container">

        <h2>Add Category:</h2>

        <form action="{{route('categories.store')}}" method="POST">

            @csrf

            @csrf
            <label>
                <span>Label category : </span>
                <input type="text" name="label" value="{{old('title')}}" placeholder="Label...">
            </label><br>

            <input type="submit" value="Create">

        </form>
    </div>
@endsection