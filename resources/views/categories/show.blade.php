@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h2>All Post in {{$category->label}}: </h2>
            <div class="col-md-8 flex-div">
                @foreach ($posts as $post)

                    <div class="card" style="width: 18rem;">
                        <div class="card-body div-body">
                            <h5 class="card-title">{{$post->title}}</h5>
                            <a href="{{route('posts.show', $post->id)}}" class="card-link"><button type="button" class="btn btn-outline-success">Show detail</button></a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection