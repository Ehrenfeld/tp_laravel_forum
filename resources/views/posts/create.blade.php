@extends('layouts.app')

@section('content')
    <div class="container">

        <h2>Add Posts: </h2>
        <form action="{{route('posts.store')}}" method="post">
            {{-- Protected field CSRF --}}
            @csrf
            <label>
                <span>Title : </span>
                <input type="text" name="title" value="{{old('title')}}">
            </label><br>
            <label>
                <span>Content : </span>
                <textarea name="content">{{old('content')}}</textarea>
            </label><br>

            <hr>

            <select name="categories">
                @foreach ($categories as $category)
                            <option value="{{$category->id}}">{{ $category->label }}</option>
                @endforeach
            </select> <br><br>

            <input type="submit" value="Submit">
        </form>

    </div>
@endsection