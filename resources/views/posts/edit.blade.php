@extends('layouts.app')


@section('content')


    <h1>Edit an Posts.</h1>

    <form action="{{route('posts.update', $post)}}" method="post">

        @csrf

        @method('PUT')
        <label>
            <span>Title : </span>
            <input type="text" name="title" value="{{ old('title') ?: $post->title}}">
        </label><br>
        <label>
            <span>Content : </span>
            <textarea name="content">{{ old('content') ?: $post->content}}</textarea>
        </label><br>

            <select name="categories">

                        @foreach ($categories as $category)
                            <option value="{{$category->id}}"
                            @if ($category->id == $post->category_id)
                                <?php echo "selected"; ?>
                            @endif>{{ $category->label }}</option>
                        @endforeach

            </select> <br><br>
        <input class="btn btn-success" type="submit" value="Update">
    </form>
@endsection