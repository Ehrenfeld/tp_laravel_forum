@extends('layouts.app')

@section('title', 'Details post ' . $post->id)

@section('header')
@endsection

@section('content')
<div class="container">
    <h3>{{$post->title}}</h3>
    <div class="card">


        <div class="card-body">

            <p>{{$post->content}}</p>
        </div>
    </div>


    <span><i>Author: {{$user->name}} </i></span><br>

    @auth()
        @if (Auth::user()->id == $post->user_id)
            <button class="btn btn-outline-success"><a href="{{route('posts.edit', $post)}}">Edit post</a></button>

            <form action="{{route('posts.destroy', $post)}}" method="post">
                @csrf
                @method('DELETE')
                <input class="btn btn-danger" type="submit" value="Delete post">
            </form>
        @endif
    @endauth

    <hr>
    <hr>
    <div>
        @if (!empty($comments))

            @foreach ($comments as $comment)
                <div class="card">
                    <div class="card-body">

                        <p class="flex-wrap">{{ \App\Comment::smiley($comment->content) }}</p>
                    </div>
            @auth()
                @if (Auth::user()->id == $comment->user_id)
                    <form method="POST" action="{{ route('comments.destroy', [ $post->id, $comment->id ]) }}">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete">
                    </form>
                @endif
            @endauth
                    <span><i>by {{$comment->user->name}}</i></span>
                </div>

            <hr>

            @endforeach
        @endif
    </div>

    @auth()
        <h4>Respond:</h4>

        <form method="POST" action="{{ route('comments.store', $post->id)}}">
            @csrf
            <textarea name="content"></textarea> <br>
            <input type="submit" value="Respond now !">
        </form>
    @endauth
</div>
@endsection